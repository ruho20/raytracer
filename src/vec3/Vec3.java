package vec3;

import java.util.Random;

public class Vec3 {

    /*
     * e[0] is the x coordinate.
     * e[1] is the y coordinate.
     * e[2] is the z coordinate.
     */

    public double[] e = new double[3];

    /**
     * Default constructor.
     */
    public Vec3() {
        e[0] = 0;
        e[1] = 0;
        e[2] = 0;
    }

    /**
     * Sets constructor parameters to e array indexes.
     *
     * @param e0 x coordinate.
     * @param e1 y coordinate.
     * @param e2 z coordinate
     */
    public Vec3(double e0, double e1, double e2) {
        e[0] = e0;
        e[1] = e1;
        e[2] = e2;
    }

    /**
     * Generates a vector with random values between the given parameter values.
     *
     * @param min Min number.
     * @param max Max number.
     * @return a vector with values between given parameter values.
     */
    public static Vec3 random(double min, double max) {
        Random rd = new Random();
        return new Vec3(rd.nextDouble(min, max), rd.nextDouble(min, max), rd.nextDouble(min, max));
    }

    /**
     * Look for a random point in the unit sphere.
     *
     * @return random point in unit sphere.
     */
    public static Vec3 randomInUnitSphere() {
        while (true) {
            Vec3 p = random(-1, 1);
            if (p.sqrtLength() >= 1)
                continue; // Generated point is outside the unit sphere, so keep trying.

            return p;
        }
    }

    /**
     * True lambertian reflection formula.
     * Generate a random unit vector placed at a random point in the unit sphere.
     *
     * @return random unit vector in sphere.
     */
    public Vec3 randomUnitVector() {
        return unitVector(randomInUnitSphere());
    }

    /**
     * Alternative diffuse formulation.
     * Makes the scatter direction point away from the hit point without with no dependence on the angle of the normal.
     *
     * @return random point in unit sphere, away from the hit point.
     */
    public Vec3 randomInHemisphere(final Vec3 normal) {
        Vec3 inUnitSphere = randomInUnitSphere();

        /* scatter direction is pointing away from the hit point */
        if (dot(inUnitSphere, normal) > 0.0)
            return inUnitSphere;

        return inUnitSphere.MultiplyByScalar(-1); // Make the scatter direction point away from the hit point.
    }

    /**
     * Returns x coordinate.
     *
     * @return value of index zero.
     */
    public final double x() {
        return e[0];
    }

    /**
     * Returns y coordinate.
     *
     * @return value of index one.
     */
    public final double y() {
        return e[1];
    }

    /**
     * Returns z coordinate.
     *
     * @return value of index two.
     */
    public final double z() {
        return e[2];
    }

    /**
     * Subtract this vector with given vector.
     *
     * @param v this vector.
     * @return calculation of two subtracted vectors in vector format.
     */
    public Vec3 Substract(Vec3 v) {
        return new Vec3(e[0] - v.e[0], e[1] - v.e[1], e[2] - v.e[2]);
    }

    public final Vec3 SubtractByScalar(double n) {
        return new Vec3(e[0] - n, e[1] - n, e[2] - n);
    }

    /**
     * Return the coordinate value by parameter value.
     *
     * @param i Index value.
     * @return x, y or z value of vector.
     */
    public final double coordinate(int i) {
        return e[i];
    }

    /**
     * Add this vector with the given vector.
     *
     * @param v Given vector.
     * @return calculation of two added vectors in vector format.
     */
    public Vec3 Add(Vec3 v) {
        return new Vec3(e[0] + v.e[0], e[1] + v.e[1], e[2] + v.e[2]);
    }

    /**
     * Multiply this vector with the given vector.
     *
     * @param v Given vector.
     * @return calculation of two multiplied vectors in vector format.
     */
    public Vec3 Multiply(Vec3 v) {
        return new Vec3(e[0] * v.e[0], e[1] * v.e[1], e[2] * v.e[2]);
    }

    /**
     * Multiply this vector by parameter value.
     *
     * @param n Value that is multiplied with this vector.
     * @return calculation of this vector multiplied by given value.
     */
    public Vec3 MultiplyByScalar(double n) {
        return new Vec3(e[0] * n, e[1] * n, e[2] * n);
    }

    /**
     * Divide this vector by given vector.
     *
     * @param v Given vector.
     * @return calculation of two divided vectors in vector format.
     */
    public final Vec3 Divide(Vec3 v) {
        return new Vec3(e[0] / v.e[0], e[1] / v.e[1], e[2] / v.e[2]);
    }

    /**
     * Divide this vector by parameter value.
     *
     * @param number The value that is divided by this vector.
     * @return calculation of this vector divided by given value.
     */
    public final Vec3 DivideByScalar(double number) {
        return new Vec3(e[0] / number, e[1] / number, e[2] / number);
    }

    /**
     * Return the length of this vector.
     *
     * @return length of this vector.
     */
    public final double length() {
        return Math.sqrt(
                Math.pow(e[0], 2.0) + Math.pow(e[1], 2.0) + Math.pow(e[2], 2.0));
    }

    /* vec3.Vec3 utility functions */

    /**
     * Return the length of this vector.
     *
     * @return length of this vector in squared format.
     */
    public final double sqrtLength() {
        return (Math.pow(e[0], 2.0) + Math.pow(e[1], 2.0) + Math.pow(e[2], 2.0));
    }

    /**
     * Normalize give vector.
     *
     * @param v Given vector.
     * @return unit of given vector in vector format.
     */
    public Vec3 unitVector(Vec3 v) {
        return v.DivideByScalar(v.length());
    }

    /**
     * Return dot product of two vectors.
     *
     * @param u Vector that will be multiplied.
     * @param v Vector that will be multiplied.
     * @return dot product of two vectors in double format.
     */
    public final double dot(final Vec3 u, final Vec3 v) {
        return u.e[0] * v.e[0] + u.e[1] * v.e[1] + u.e[2] * v.e[2];
    }

    /**
     * Return dot product of one vector.
     *
     * @param v Given vector.
     * @return dot product of given vector in double format.
     */
    public final double dotVector(final Vec3 v) {
        return e[0] * v.e[0] + e[1] * v.e[1] + e[2] * v.e[2];
    }

    /**
     * Prevent zero scatter direction problem if the generated unit vector is opposite to the normal vector.
     *
     * @return true if the two vectors add up to zero in all dimensions.
     */
    public final boolean nearZero() {
        final double s = 1e-8; // naar kijken.

        return (Math.abs(e[0]) < s && Math.abs(e[1]) < s && Math.abs(e[2]) < s);
    }

    /**
     * Reflection formula
     *
     * @param v vector.
     * @param n The unit vector.
     * @return reflected vector.
     */
    public Vec3 reflect(final Vec3 v, final Vec3 n) {
        // v - 2*dot(v,n)*n;

        return v.Substract(n.MultiplyByScalar(2 * dot(v, n)));
    }

    public void setValues(Vec3 values) {
        this.e[0] = values.e[0];
        this.e[1] = values.e[1];
        this.e[2] = values.e[2];
    }
}