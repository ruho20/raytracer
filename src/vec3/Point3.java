package vec3;

public class Point3 extends Vec3 {

    /* Set constructor parameters to super class vec3.Vec3 */

    /**
     * Set constructor parameter values to super class.
     *
     * @param x X coordinate value.
     * @param y Y coordinate value.
     * @param z Z coordinate value.
     */
    public Point3(double x, double y, double z) {
        super(x, y, z);
    }

    /* Override vec3.Vec3 methods */

    @Override
    public final Point3 Add(Vec3 v) {
        return new Point3(e[0] + v.e[0], e[1] + v.e[1], e[2] + v.e[2]);
    }

    @Override
    public final Point3 Substract(Vec3 v) {
        return new Point3(e[0] - v.e[0], e[1] - v.e[1], e[2] - v.e[2]);
    }
}
