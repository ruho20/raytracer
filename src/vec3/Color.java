package vec3;

import main.HitRecord;
import main.Hittable;
import main.Ray;

/**
 * Used for generating or modifying colors. Since Color class uses the same properties as Vec3, I decided to extend
 * this class from Vec3.
 */
public class Color extends Vec3 {

    /**
     * Set constructor parameter values to super class fields.
     *
     * @param r Red value.
     * @param g Green value.
     * @param b Blue value.
     */
    public Color(double r, double g, double b) {
        super(r, g, b);
    }

    /**
     * Convert pixel RGB value combinations to a single integer value.
     *
     * @param pixelColor      Given color value in rgb.
     * @param samplesPerPixel Amount of samples per pixel.
     * @return a combination of R, G and B values.
     */
    public final int writeColor(Color pixelColor, int samplesPerPixel) {
        double r = pixelColor.x();
        double g = pixelColor.y();
        double b = pixelColor.z();

        double scale = 1.0 / samplesPerPixel; // Divide de color by number of samples.

        /* Gamma correct color values. Gamma = 2.0 */

        r = Math.sqrt(scale * r);
        g = Math.sqrt(scale * g);
        b = Math.sqrt(scale * b);

        /* Translate values of each color value. */

        int ir = (int) (256 * clamp(r));
        int ig = (int) (256 * clamp(g));
        int ib = (int) (256 * clamp(b));

        /* Return a single combination value of R, G and b by bit shifting them to the left. */
        return (ir << 16) + (ig << 8) + ib;
    }

    /**
     * Checks if a ray hits something in the scene and if true, calls itself again with a random diffuse bounce ray.
     * This keeps on going until either the ray bounce limit is reached, or the given ray does not hit any sphere in
     * the scene.
     *
     * @param r     Given ray.
     * @param scene Scene that could contain sphere objects.
     * @param depth Recursion depth.
     * @return color of a ray.
     */
    public final Color rayColor(final Ray r, final Hittable scene, int depth) {
        HitRecord rec = new HitRecord();

        if (depth <= 0) // Keep track of ray bounce limit.
            return new Color(0, 0, 0);

        // Check if ray hits something in the scene and remove hits very near zero to prevent shadow acne problem. */
        if (scene.hit(r, 0.001, Double.MAX_VALUE, rec)) {
            Ray scattered = new Ray();
            Color attenuation = new Color(0, 0, 0);

            /* Use the correct scatter formula for the current material object, and keep multiplying the attenuation
             * by calling the rayColor method again, until either the bounce limit has reached, or the scatter formula
             * returns false.
             */
            if (rec.material.scatter(r, rec, attenuation, scattered))
                return attenuation.Multiply(rayColor(scattered, scene, depth - 1));

            return new Color(0, 0, 0);
        }

        Vec3 unitDirection = unitVector(r.direction()); // Normalize given vector.

        // if t is smaller than one, color will be blue. If t is smaller than zero, color will be white
        double t = (unitDirection.y() + 1) * 0.5;

        Color color1 = new Color(1.0, 1.0, 1.0);
        Color color2 = new Color(0.5, 0.7, 1.0);

        return color1.MultiplyByScalar(1.0 - t).Add(color2.MultiplyByScalar(t));
    }

    /**
     * Clamp the value x to [min,max].
     *
     * @param x R, G or B value
     * @return the given x , 0.0, 0.999 depending x value.
     */
    private double clamp(double x) {
        if (x < 0.0) return 0.0;

        return Math.min(x, 0.999);
    }

    /**
     * Override MultiplyByScalar method and return vec3.Color type instead of vec3.Vec3 type,
     *
     * @param n Value that is multiplied with this vector.
     * @return calculation of this vector multiplied by given value.
     */
    @Override
    public final Color MultiplyByScalar(double n) {
        return new Color(e[0] * n, e[1] * n, e[2] * n);
    }

    /**
     * Override Add method and return vec3.Color type instead of vec3.Vec3 type.
     *
     * @param v Given vector.
     * @return calculation of two added vectors in vector format.
     */
    @Override
    public final Color Add(Vec3 v) {
        return new Color(e[0] + v.e[0], e[1] + v.e[1], e[2] + v.e[2]);
    }

    /**
     * Override Multiply method and return vec3.Color type instead of vec3.Vec3 type.
     *
     * @param v Given vector.
     * @return Multiplication result returned as color type.
     */
    @Override
    public final Color Multiply(Vec3 v) {
        return new Color(e[0] * v.e[0], e[1] * v.e[1], e[2] * v.e[2]);
    }
}
