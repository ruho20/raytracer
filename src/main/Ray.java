package main;

import vec3.Point3;
import vec3.Vec3;

/* P(t) = vec3.Point3 + t * direction  */

public class Ray {
    Point3 origin;
    Vec3 direction;

    /**
     * Create a mew main.Ray object that has a starting value and a direction value.
     *
     * @param origin    starting value of the ray.
     * @param direction direction value of the ray.
     */
    public Ray(final Point3 origin, final Vec3 direction) {
        this.origin = origin;
        this.direction = direction;
    }

    public Ray() {
    }

    /**
     * Return the origin of this ray.
     *
     * @return origin vec3.Point3 object.
     */
    public final Point3 origin() {
        return this.origin;
    }

    /**
     * Return the direction of the ray.
     *
     * @return direction vec3.Vec3 type.
     */
    public final Vec3 direction() {
        return this.direction;
    }

    /**
     * Set the origin of the ray.
     *
     * @param origin The starting point.
     */
    public final void setOrigin(Point3 origin) {
        this.origin = origin;
    }

    /**
     * Set the direction of the ray.
     *
     * @param direction The moving direction.
     */
    public void setDirection(Vec3 direction) {
        this.direction = direction;
    }

    /**
     * Return the location of the ray.
     *
     * @param t The location value.
     * @return location vec3.Point3 type.
     */
    public final Point3 at(double t) {
        return this.origin.Add(this.direction.MultiplyByScalar(t));
    }
}
