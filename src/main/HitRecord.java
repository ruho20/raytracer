package main;

import material.Material;
import vec3.Point3;
import vec3.Vec3;

public class HitRecord {
    public Point3 p;
    public Vec3 normal;
    public Material material;
    public double t;
    public boolean fontFace;

    /**
     * Set values of hit record object to default zero values.
     */
    public HitRecord() {
        t = 0;
        p = new Point3(0,0,0);
        normal = new Vec3(0,0,0);
    }

    /**
     * Determine which side of the surface the ray is coming from.
     * @param r Give ray.
     * @param outwardNormal given outward normal.
     */
    public void setFaceNormal(final Ray r, final Vec3 outwardNormal) {

        /* Make normals always point outward to determine the time of intersection.  */

        fontFace = outwardNormal.dotVector(r.direction()) < 0; // Check if outward normal is pointing against the ray.

        // Make the outward normal point against the ray (outward) if the outward normal is with the ray.
        normal = fontFace ? outwardNormal : outwardNormal.MultiplyByScalar(-1);
    }
}
