package main;

public interface Hittable {

    /**
     * Checks if a ray hits something in the scene.
     * @param r Radius of the sphere.
     * @param tMin Interval value
     * @param tMax Interval value
     * @param rec hit record data
     * @return true if sphere is hit or false if not.
     */
    boolean hit(final Ray r, double tMin, double tMax, HitRecord rec);
}
