package main;

import material.Lambertian;
import material.Metal;
import vec3.Color;
import vec3.Point3;

import javax.swing.JFrame;
import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class Main {
    public static void main(String[] args) {

        // define image height and width.
        final double aspectRatio = 16.0 / 9.0;
        final int imageWidth = 720;
        final int imageHeight = (int)(imageWidth / aspectRatio);

        // Samples per pixel.
        final int samplesPerPixel = 100;

        // Max ray bounce limit.
        int maxDepth = 50;

        Lambertian materialGround = new Lambertian(new Color(0.8, 0.8, 0.0));
        Lambertian materialCenter = new Lambertian(new Color(0.7, 0.3, 0.3));
        Metal materialLeft = new Metal(new Color(0.8, 0.8, 0.8));
        Metal materialRight = new Metal(new Color(0.8, 0.6, 0.2));

        // Create a scene.
        HittableList scene = new HittableList();

        scene.add(new Sphere(new Point3(0.0, -100.5, -1.0), 100.0, materialGround));
        scene.add(new Sphere(new Point3(0.0, 0.0, -1.0), 0.5, materialCenter));
        scene.add(new Sphere(new Point3(-1.0, -0.0, -1.0), 0.5, materialLeft));
        scene.add(new Sphere(new Point3(1.0, 0.0, -1.0), 0.5, materialRight));


        JFrame frame = new JFrame("Raytracer"); // Create new JFrame object.
        Canvas canvas = new Canvas(); // Create canvas object to draw image in.

        // frame setup + add the canvas to it.
        frame.add(canvas);
        frame.setSize(imageWidth, imageHeight);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setResizable(true);
        frame.setVisible(true);

        Camera camera = new Camera();

        /*
         * The BufferStrategy class represents the mechanism with which to organize complex memory on a particular
         * Canvas or window.
         */

        canvas.createBufferStrategy(2); // General double-buffering strategy.

        /* Create Graphics context for the drawing buffer. */

        Graphics graphics = canvas.getBufferStrategy().getDrawGraphics();
        BufferedImage buffer = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);


        /* Send rays into the scene */

        for (int j = imageHeight - 1; j >= 0; --j) {
            for (int i = 0; i < imageWidth; ++i) {

                Color pixelColor = new Color(0, 0, 0);

                for (int s = 0; s < samplesPerPixel; ++s) {
                    double u = (i + Math.random()) / (imageWidth - 1);
                    double v = (j + Math.random()) / (imageHeight - 1);

                    Ray r = camera.getRay(u, v);
                    pixelColor = pixelColor.Add(pixelColor.rayColor(r, scene, maxDepth));
                }

                /*
                 * Parameter description
                 * x: The X coordinate of the pixel to set.
                 * y: The y coordinate of the pixel to set.
                 * rgb: The rgb value
                 */

                buffer.setRGB(i, imageHeight - j - 1, pixelColor.writeColor(pixelColor, samplesPerPixel));
            }
            graphics.drawImage(buffer, 0, 0, frame.getWidth(), frame.getHeight(), null);
            canvas.getBufferStrategy().show();
        }

    }
}