package main;

import vec3.Point3;
import vec3.Vec3;

/**
 * Camera class
 */
public class Camera {

    private final Point3 origin;
    private final Point3 lowerLeftCorner;
    private final Vec3 horizontal;
    private final Vec3 vertical;

    /**
     * Configure camera object when initialized.
     */
    public Camera() {
        final double aspectRatio = 16.0 / 9.0; // Length height.
        double viewportHeight = 2.0; // Amount of height the camera sees.
        double viewportWidth = aspectRatio * viewportHeight; // Amount of width the camera sees.
        double focalLength = 1.0;  // Amount of distance between te "sensor", and "lens" of the camera.

        origin = new Point3(0, 0, 0);

        /* Calculate screen from upper left hand corner, and place it on -1 Z axis. */
        horizontal = new Vec3(viewportWidth, 0, 0);
        vertical = new Vec3(0, viewportHeight, 0);
        lowerLeftCorner = origin
                .Substract(horizontal.DivideByScalar(2))
                .Substract(vertical.DivideByScalar(2))
                .Substract(new Vec3(0, 0, focalLength));
    }

    /**
     * Return the origin and direction of a ray.
     * @param u Origin.
     * @param v Direction.
     * @return origin and direction of a ray.
     */
    public final Ray getRay(double u, double v) {
        return new Ray(origin, lowerLeftCorner
                .Add(horizontal.MultiplyByScalar(u))
                .Add(vertical.MultiplyByScalar(v))
                .Substract(origin));
    }
}
