package main;

import material.Material;
import vec3.Point3;
import vec3.Vec3;

/**
 * Used as an object in the "world".
 */
public class Sphere implements Hittable {
    public Material material;
    Point3 center;
    double radius;

    /**
     * Set defined parameters of sphere object.
     *
     * @param center   Center of sphere.
     * @param radius   Radius of sphere.
     * @param material Reference to the material of the sphere.
     */
    public Sphere(Point3 center, double radius, Material material) {
        this.center = center;
        this.radius = radius;
        this.material = material;
    }

    @Override
    public boolean hit(Ray r, double tMin, double tMax, HitRecord rec) {
        Vec3 oc = r.origin().Substract(center);

        double a = r.direction().sqrtLength();
        double halfB = oc.dotVector(r.direction()); // Keep in mind.
        double c = oc.sqrtLength() - radius * radius;

        double discriminant = halfB * halfB - a * c;

        if (discriminant < 0) return false;

        double sqrt = Math.sqrt(discriminant);

        // Find the nearest root that lies in the acceptable range.

        double root = (-halfB - sqrt) / a;

        if (root < tMin || tMax < root) {
            root = (-halfB + sqrt) / a;

            if (root < tMin || tMax < root) return false;
        }

        rec.t = root;
        rec.p = r.at(rec.t);

        Vec3 outwardNormal = (rec.p.Substract(center)).DivideByScalar(radius);

        rec.setFaceNormal(r, outwardNormal);
        rec.material = material; // Set hit record material to sphere material reference.

        return true;
    }
}