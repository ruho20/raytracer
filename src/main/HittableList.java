package main;

import java.util.ArrayList;
import java.util.List;

/**
 * Used as the "world" that contains objects (spheres).
 */
public class HittableList implements Hittable {

    public List<Hittable> objects;

    /**
     * Initialize objects list when creating hittable list object.
     */
    public HittableList() {
        objects = new ArrayList<>();
    }

    /**
     * Initialize objects list and add the first object to the list if the user wants to.
     * @param object Object in the scene.
     */
    public HittableList(Hittable object) {
        objects = new ArrayList<>();
        objects.add(object);
    }


    /**
     * Remove all items in list.
     */
    public void clear() {
        objects.clear();
    }

    /**
     * Add an item to the list.
     * @param object Object in the scene.
     */
    public void add(Hittable object) {
        objects.add(object);
    }

    @Override
    public boolean hit(Ray r, double tMin, double tMax, HitRecord rec) {
        HitRecord tempRec = new HitRecord();
        boolean hitAnything = false;
        double closestSoFar = tMax;

        for (final Hittable object : objects) {
            if (object.hit(r, tMin, closestSoFar, tempRec)) {
                hitAnything = true;
                closestSoFar = tempRec.t;
                rec.t = tempRec.t;
                rec.normal = tempRec.normal;
                rec.p = tempRec.p;
                rec.material = tempRec.material;
            }
        }

        return hitAnything;
    }
}
