package material;

import main.HitRecord;
import main.Ray;
import vec3.Color;
import vec3.Vec3;

/**
 * Lambertian material.
 */
public class Lambertian extends Material {

    /**
     * Create Lambertian (diffuse) object with given reflection color.
     *
     * @param color Color of the reflection.
     */
    public Lambertian(final Color color) {
        this.albedo = color;
    }

    /**
     * Generate a scattered ray from the point that was hit, add a random direction and set the weakened light (albedo)
     * the attenuation.
     *
     * @param rIn         Ray that hit the sphere.
     * @param rec         info about the current hit.
     * @param attenuation Attenuated color.
     * @param scattered   Scattered ray.
     * @return new scattered from the point that was hit by the ray.
     */
    @Override
    public boolean scatter(final Ray rIn, final HitRecord rec, Color attenuation, Ray scattered) {

        // Generate a random vector in the sphere and get the direction of it.
        Vec3 scatterDirection = rec.normal.Add(new Vec3().randomUnitVector());

        if (scatterDirection.nearZero())
            scatterDirection = rec.normal;

        // Generate a new scattered ray from the point that was hit by the ray, and give the direction.
        scattered.setOrigin(rec.p);
        scattered.setDirection(scatterDirection);

        attenuation.setValues(albedo);

        return true;
    }
}
