package material;

import main.HitRecord;
import main.Ray;
import vec3.Color;

/**
 * Material class used by different kinds of material.
 */
public abstract class Material {

    public Color albedo;

    public abstract boolean scatter(final Ray rIn, final HitRecord rec, Color attenuation, Ray scattered);
}