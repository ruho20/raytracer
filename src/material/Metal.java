package material;

import main.HitRecord;
import main.Ray;
import vec3.Color;
import vec3.Vec3;

public class Metal extends Material {

    public Metal(final Color color) {
        albedo = color;
    }

    /**
     * Scatter formula for metal materials.
     *
     * @param rIn         Ray that hits the object.
     * @param rec         Information about the current hit.
     * @param attenuation Amount of color loss.
     * @param scattered   the scattered ray.
     * @return true if reflectance is bigger than zero.
     */
    @Override
    public boolean scatter(final Ray rIn, final HitRecord rec, Color attenuation, Ray scattered) {
        Vec3 vector = new Vec3();
        Vec3 reflected = vector.reflect(vector.unitVector(rIn.direction()), rec.normal);

        /* Generate a new scattered ray from the point that was hit by the ray, and give the direction. */

        scattered.setOrigin(rec.p);
        scattered.setDirection(reflected);

        attenuation.setValues(albedo);

        return (vector.dot(scattered.direction(), rec.normal) > 0);
    }
}
